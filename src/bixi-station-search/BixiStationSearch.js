import React from 'react';
import './BixiStationSearch.scss';
import BixiStationDetail from "../bixi-station-detail/bixiStationDetail";
export class BixiStationSearch extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
        this.items = this.props.bixiData;
        this.state = {
            suggestions: [],
            currentStation: null
        }
    }

    onTextChange = (e) => {
        const search = e?.target?.value;
        let suggestions = [];
        if (!!search && search?.length > 0) {
            suggestions = this.items
                .sort((station1, station2) => station1.s - station2.s)
                .filter(value => value?.s?.includes(search));
        } else {
            this.setState(() => ({ currentStation: null }))
        }
        this.setState(() => ({ suggestions, text: search }));

    };

    suggestionSelected(value) {
        this.setState(() => ({
            text: value?.s,
            suggestions: [],
            currentStation: {
                name: value?.s,
                longitude: value?.lo,
                latitude: value?.la
            }
        }))
    }

    renderSuggestionList() {
        const { suggestions } = this.state;

        if (suggestions?.length === 0) {
            return null;
        }
        return (<div className="form-suggestions">
            {suggestions.map((selection, index) => <div className="form-suggestions-item" onClick={() => this.suggestionSelected(selection)} key={index}>{selection?.s}</div>)}
        </div>)
            ;
    }

    render() {
        const { text } = this.state;
        const { currentStation } = this.state;
        return (
            <div>
                <p className="title">Find a BIXI station by its name :</p>
                <form className="form">
                    <div className="form-autocomplete">
                        <input value={text || ''} className="form-autocomplete-input" type="text" autoComplete="off" onChange={this.onTextChange} name="search" id="search" />
                        {(this.renderSuggestionList())}
                    </div>
                </form>
                <div className="form-station-name">
                    <strong>Station Selected</strong> : {this.state.currentStation?.name || 'No one'}
                </div>
                <div>
                    <BixiStationDetail selectedStation={currentStation} />
                </div>
            </div>
        );
    }
}

export default BixiStationSearch;