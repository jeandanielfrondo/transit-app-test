import logo from './logo.svg';
import './App.css';
import BixiStationSearch from './bixi-station-search/BixiStationSearch';
import * as bixiData from './bixi-data.json'
function App() {

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

      </header>
      <div className="App-content">
        <BixiStationSearch bixiData={bixiData?.default} />
      </div>

    </div>
  );
}

export default App;
