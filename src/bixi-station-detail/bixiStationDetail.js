import React from 'react';
import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';

export class BixiStationDetail extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    onMarkerClick = (props, marker, e) =>
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });

    onClose = props => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    };

    render() {
        const mapStyles = {
            width: '100%',
            height: '100%',
            position: 'relative'
        };
        return !!this.props.selectedStation ? (
            <Map
                google={this.props.google}
                zoom={8}
                style={mapStyles}
                initialCenter={{ lat: this.props.selectedStation?.latitude, lng: this.props.selectedStation?.longitude }}
            >
                <Marker
                    onClick={this.onMarkerClick}
                    name={this.props.selectedStation?.name}
                    position={{ lat: this.props.selectedStation?.latitude, lng: this.props.selectedStation?.longitude }} />
                <InfoWindow
                    marker={this.state?.activeMarker}
                    visible={this.state?.showingInfoWindow}
                    onClose={this.onClose}
                >
                    <div>
                        <h4>{this.props.selectedStation?.name}</h4>
                    </div>
                </InfoWindow>
            </Map>

        ) : null;
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyA7_CDJY16v27LPQ_J_KdHCEGuwUR2uvB0'
})(BixiStationDetail);
